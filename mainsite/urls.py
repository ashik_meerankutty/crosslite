from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'about/$', views.about, name='about'),
    url(r'services/$', views.services, name='services'),
    url(r'contact/$', views.contact, name='contact'),
    url(r'blog/$', views.blog, name='blog'),
    url(r'portfolio/$', views.projects, name='blog'),
    url(r'gallery/$', views.gallery, name='gallery'),
    url(r'^blog/(?P<BlogPost_id>[0-9]+)/$', views.detail, name='detail'),
    url(r'^portfolio/(?P<Post_id>[0-9]+)/$', views.projectdetail, name='projectdetail'),
    url(r'^gallery/(?P<Album_id>[0-9]+)/$', views.photos, name='photos'),
]
