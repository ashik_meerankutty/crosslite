# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2017-04-19 14:34
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('mainsite', '0006_photo'),
    ]

    operations = [
        migrations.AddField(
            model_name='photo',
            name='pub_date',
            field=models.DateTimeField(default=django.utils.timezone.now, verbose_name=b'date published'),
            preserve_default=False,
        ),
    ]
