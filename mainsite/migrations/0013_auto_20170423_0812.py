# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2017-04-23 08:12
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mainsite', '0012_auto_20170423_0525'),
    ]

    operations = [
        migrations.AddField(
            model_name='post',
            name='image1',
            field=models.FileField(blank=True, null=True, upload_to=b''),
        ),
        migrations.AddField(
            model_name='post',
            name='image2',
            field=models.FileField(blank=True, null=True, upload_to=b''),
        ),
        migrations.AddField(
            model_name='post',
            name='image3',
            field=models.FileField(blank=True, null=True, upload_to=b''),
        ),
        migrations.AddField(
            model_name='post',
            name='image4',
            field=models.FileField(blank=True, null=True, upload_to=b''),
        ),
    ]
