from django.db import models

# Create your models here.


class Notification(models.Model):
    notification_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')

    def __str__(self):
        return self.notification_text

class Post(models.Model):
    title = models.CharField(max_length = 200)
    image = models.FileField()
    image1 = models.FileField()
    image2 = models.FileField()
    image3 = models.FileField()
    image4 = models.FileField()
    content = models.TextField()
    area = models.CharField(max_length = 200)
    time = models.CharField(max_length = 200)
    cost = models.CharField(max_length = 200)
    projectType = models.CharField(max_length = 200)
    projectCategory = models.CharField(max_length = 200)
    projectLocation = models.CharField(max_length = 200)
    pub_date = models.DateTimeField('date published')


    def __str__(self):
        return self.title
class Album(models.Model):
    title = models.CharField(max_length = 200)
    image = models.FileField(null = False, blank = True)
    pub_date = models.DateTimeField('date published')

    def __str__(self):
        return self.title
                       
class Photo(models.Model):
    title = 'photo'
    image = models.FileField(null = False, blank = True)
    album = models.ForeignKey(Album, on_delete=models.CASCADE)

    def __str__(self):
        return self.title
class BlogPost(models.Model):
    title = models.CharField(max_length = 200)
    author = models.CharField(max_length = 200)
    image = models.FileField(null = True,blank =True) 
    content = models.TextField()
    pub_date = models.DateTimeField('date published')

    def __str__(self):
        return self.title       