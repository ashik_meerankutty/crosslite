from django.contrib import admin

# Register your models here.

from .models import Notification
from .models import Post, Photo, BlogPost,Album

admin.site.register(Post)
admin.site.register(Notification)
admin.site.register(Photo)
admin.site.register(BlogPost)
admin.site.register(Album)