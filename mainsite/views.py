from django.shortcuts import render
from django.http import HttpResponse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import Notification,Post,Photo,BlogPost,Album
from django.http import Http404


def index(request):
    latest_notifications = Notification.objects.order_by('-pub_date')[:5]
    context = {'latest_notifications':latest_notifications}

    return render(request, 'mainsite/index.html', context)

def about(request):
    return render(request, 'mainsite/about.html')

def services(request):
    return render(request, 'mainsite/services.html')

def contact(request):
    return render(request, 'mainsite/contact.html')

def projects(request):
    posts = Post.objects.order_by('-pub_date')
    context = {'posts': posts}
    return render(request, 'mainsite/portfolio.html',context)
def gallery(request):
    album = Album.objects.order_by('-pub_date')
    context = {'photos': album}
    return render(request, 'mainsite/gallery.html',context) 
def blog(request):
    posts = BlogPost.objects.order_by('-pub_date')
    paginator = Paginator(posts, 10) # Show 25 contacts per page

    page = request.GET.get('page')
    try:
        post = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        post = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        post = paginator.page(paginator.num_pages)
    latest_posts = BlogPost.objects.order_by('-pub_date')[:5]
    return render(request, 'mainsite/blog.html',{'blog': post,'latest_posts':latest_posts,})    
def detail(request, BlogPost_id):
    post = BlogPost.objects.get(pk=BlogPost_id)
    latest_posts = BlogPost.objects.order_by('-pub_date')[:5]
    return render(request, 'mainsite/details.html',{'post': post,'latest_posts':latest_posts,})    

def projectdetail(request, Post_id):   
    post = Post.objects.get(pk=Post_id)
    return render(request, 'mainsite/project.html',{'post': post,}) 

def photos(request,Album_id):
    album = Album.objects.get(pk=Album_id)
    return render(request, 'mainsite/imagegallery.html',{'album': album,})  
def error404(request):
    return HttpResponse("404 PAGE NOT FOUND")       

